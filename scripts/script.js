//==============================================================================
// Welcome to scripting in Spark AR Studio! Helpful links:
//
// Scripting Basics - https://fb.me/spark-scripting-basics
// Reactive Programming - https://fb.me/spark-reactive-programming
// Scripting Object Reference - https://fb.me/spark-scripting-reference
// Changelogs - https://fb.me/spark-changelog
//==============================================================================

// How to load in modules
const Animation = require('Animation');
const Diagnostics = require('Diagnostics');
const Scene = require('Scene');
const TouchGestures = require('TouchGestures');
const Time = require('Time');
const Reactive = require('Reactive');

const limit = 175;
const speed = 5;
var dir = -1;


// How to access scene objects
const player = Scene.root.find('star');
const playerTransform = player.transform;

Diagnostics.log('test start');

Time.ms.interval(34).subscribe(
    function(elapsedTime) {
    	update_player();
        //Diagnostics.log(" elapsed time: " + elapsedTime);
    });

function update_player() {
	if(playerTransform.x.pinLastValue() <= -limit || playerTransform.x.pinLastValue() >= limit)
    	{
    		dir = dir*-1;
    	}

    	const lastPositionX = playerTransform.x.pinLastValue();
	  	const newPositionX = Reactive.add(lastPositionX, dir*speed);
	  	playerTransform.x = newPositionX;
}

Diagnostics.log('test finish');
